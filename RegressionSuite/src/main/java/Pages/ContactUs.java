package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ContactUs 
{
	WebDriver localDriver;
	
	public ContactUs(WebDriver driver) 
	{
		 localDriver = driver;
	}

	@FindBy(how = How.XPATH, using = "//ul[@id='menu-tt-menu-1']/li[5]/a")
	WebElement contactuslink;

	@FindBy(how = How.XPATH, using = "//strong[text()='Contact Us']")
	WebElement logocontactus;

	@FindBy(how = How.XPATH, using = "//*[text()='Techno Tutors']")
	WebElement technotext;

	@FindBy(how = How.XPATH, using = "//*[text()=' 55 Town Centre Court, Suite 102 Toronto, ON M1P 4X4']")
	WebElement adresstext;

	@FindBy(how = How.XPATH, using = "//*[text()='info@technotutors.com']")
	WebElement emailtext;

	@FindBy(how = How.XPATH, using = "//*[text()=' 416-279-1947']")
	WebElement phonenumtext;

	@FindBy(how = How.XPATH, using = "//input[@name='text-625']")
	WebElement Name1;

	@FindBy(how = How.XPATH, using = "//input[@name='email-982']")
	WebElement Email1;

	@FindBy(how = How.XPATH, using = "//input[@name='text-org']")
	WebElement Subject1;

	@FindBy(how = How.XPATH, using = "//*[@name='textarea-841']")
	WebElement Message1;

	@FindBy(how = How.XPATH, using = "//*[@class='wpcf7-form-control wpcf7-submit']")
	WebElement Send;

	public String ContactUsPageTitle() 
	{
		// TODO Auto-generated method stub
		return localDriver.getTitle();
	} 

}