package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Cart 
{
	WebDriver localDriver;
	
	public Cart(WebDriver driver) 
	{
		 localDriver = driver;
	}
	
	@FindBy(how = How.XPATH, using = "//ul[@id='menu-tt-menu-1']/li[6]/a")
	WebElement cartlink;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/shop/']")
	WebElement ReturntoShop;

	@FindBy(how = How.XPATH, using = "//a[@href='/shop/?add-to-cart=1609']")
	WebElement Acadamiccart;

	@FindBy(how = How.XPATH, using = "//a[@href='/shop/?add-to-cart=1610']")
	WebElement Learnercart;

	@FindBy(how = How.XPATH, using = "(//a[@href='https://www.technotutors.ca/cart/'])[3]")
	WebElement viewcart;

	@FindBy(how = How.XPATH, using = "//input[@name='coupon_code']")
	WebElement CouponCode;

	@FindBy(how = How.XPATH, using = "//button[@name='apply_coupon']")
	WebElement ApplyCoupon;

	@FindBy(how = How.XPATH, using = "//*[@class='woocommerce-error']")
	WebElement Error;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	WebElement Quantity;

	@FindBy(how = How.XPATH, using = "//button[@name='update_cart']")
	WebElement UpdateCart;

	@FindBy(how = How.XPATH, using = "(//a[@class='remove'])[2]")
	WebElement cancel;

	@FindBy(how = How.XPATH, using = "//td[@data-title='Subtotal']")
	WebElement Subtotal;

	@FindBy(how = How.XPATH, using = "//a[@class='checkout-button button alt wc-forward']")
	WebElement CheckOut;

	// RETURNING CUSTOMER

	@FindBy(how = How.XPATH, using = "//a[@class='showlogin']")
	WebElement ReturningCustomer;

	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	WebElement UserName;

	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	WebElement Password;

	@FindBy(how = How.XPATH, using = "//ul[@class='woocommerce-error']")
	WebElement invallidemail;

	@FindBy(how = How.XPATH, using = "//input[@id='rememberme']")
	WebElement RememberMe;

	@FindBy(how = How.XPATH, using = "//button[@name='login']")
	WebElement send;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/my-account/lost-password/']")
	WebElement Forgetpass;

	@FindBy(how = How.XPATH, using = "//input[@id='user_login']")
	WebElement forgetpassemail;

	@FindBy(how = How.XPATH, using = "//button[@value='Reset password']")
	WebElement Resetpass;

	@FindBy(how = How.XPATH, using = "//ul[@class='woocommerce-error']")
	WebElement ResetpassError;

	// COUPON FOR CHECKOUT PAGE

	@FindBy(how = How.XPATH, using = "//a[@class='showcoupon']")
	WebElement couponlink;

	@FindBy(how = How.XPATH, using = "//input[@id='coupon_code']")
	WebElement insertcoupon;

	@FindBy(how = How.XPATH, using = "//button[@name='apply_coupon']")
	WebElement applycheckoutcoupon;

	@FindBy(how = How.XPATH, using = "//ul[@role='alert']")
	WebElement couponerror;

	@FindBy(how = How.XPATH, using = "//ul[@class='woocommerce-error']/li")
	WebElement wrongecouponerror;

	// BILLING DETAILS

	@FindBy(how = How.XPATH, using = "//input[@id='billing_first_name']")
	WebElement Firstname;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_last_name']")
	WebElement Lastname;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_company']")
	WebElement Comapany;

	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-input-wrapper']/strong")
	WebElement Contry;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_address_1']")
	WebElement Streetname;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_address_2']")
	WebElement apparment;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_city']")
	WebElement Town;

	@FindBy(how = How.XPATH, using = "//span[@id='select2-billing_state-container']")
	WebElement Province;

	@FindBy(how = How.XPATH, using = "//input[@class='select2-search__field']")
	WebElement InputProvince;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_postcode']")
	WebElement postalcode;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_phone']")
	WebElement phone;

	@FindBy(how = How.XPATH, using = "//input[@id='billing_email']")
	WebElement email;

	@FindBy(how = How.XPATH, using = "//input[@id='createaccount']")
	WebElement createaccount;

	@FindBy(how = How.XPATH, using = "//input[@id='account_password']")
	WebElement passwordaccount;

	@FindBy(how = How.XPATH, using = "//div[@class='woocommerce-password-strength short']")
	WebElement weakpass;

	@FindBy(how = How.XPATH, using = "//textarea[@id='order_comments']")
	WebElement otherinfo;

	@FindBy(how = How.XPATH, using = "//td[@class='product-name']")
	WebElement productname;

	@FindBy(how = How.XPATH, using = "//tr[@class='order-total']")
	WebElement total;

	@FindBy(how = How.XPATH, using = "//div[@class='woocommerce-checkout-payment']")
	WebElement Payment;

	@FindBy(how = How.XPATH, using = "//label[@for='payment_method_stripe']")
	WebElement Label;

	@FindBy(how = How.XPATH, using = "//button[@id='place_order']")
	WebElement placeorder;

	@FindBy(how = How.XPATH, using = "(//div[@class='woocommerce-notices-wrapper'])[1]")
	WebElement Errors;
	
	public String CartPageTitle() 
	{
		// TODO Auto-generated method stub
		return localDriver.getTitle();
	} 

}

